package main

import (
	"fmt"
	"log"
	"math/rand"

	"github.com/bwmarrin/discordgo"
)

func main() {

	const TOKEN string = "YOUR_TOKEN_HERE"
	const appID string = "YOUR_APPID_HERE"

	bot, e := discordgo.New("Bot " + TOKEN)
	if e != nil {
		log.Fatal(e)
	}

	fmt.Println("what")

	bot.AddHandler(replay)
	bot.AddHandler(runCommand)

	bot.AddHandlerOnce(func(s *discordgo.Session, _ *discordgo.Ready) {
		addCommand(s, appID)
	})

	bot.Identify.Intents = discordgo.IntentsAllWithoutPrivileged

	e = bot.Open()
	if e != nil {
		log.Fatal(e)
	}

	defer bot.Close()
	fmt.Println("yay")

	for true {

	}
}

func runCommand(s *discordgo.Session, i *discordgo.InteractionCreate) {

	switch i.Interaction.ApplicationCommandData().Name {

	case "hi":
		resp := discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "go away, " + i.Member.User.Username + "!",
			},
		}
		s.InteractionRespond(i.Interaction, &resp)

	case "goodbye":
		resp := discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "finally!",
			},
		}
		s.InteractionRespond(i.Interaction, &resp)
	}
}

func addCommand(s *discordgo.Session, appID string) {
	commands := []*discordgo.ApplicationCommand{
		{
			Name:        "Hi :)",
			Description: "Say hello!",
			Type:        discordgo.ChatApplicationCommand,
		},
		{
			Name:        "Goodbye :(",
			Description: "Say goodbye!",
			Type:        discordgo.ChatApplicationCommand,
		},
	}
	for i := 0; i < len(s.State.Guilds); i++ {
		s.ApplicationCommandBulkOverwrite(appID, s.State.Guilds[i].ID, commands)

	}

}

func replay(s *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Author.ID == s.State.User.ID {
		return
	}

	var goAway = [5]string{"go away", "go!", "leave me alone", "go away, " + message.Author.Username + "!", "go to a different chat"}

	if message.GuildID == "" {
		s.ChannelMessageSendReply(message.ChannelID, goAway[rand.Intn(5)], message.Reference())
	}
}
